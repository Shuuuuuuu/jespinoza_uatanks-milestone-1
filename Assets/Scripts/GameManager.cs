﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance; //Call in the GameManager and its reference is instance

    void Awake() //void Awake to see if our GameManager Works
    {
        if(instance == null) //Call and if statement to null the instance
        {
            instance = this;
        }

        else //Call else statement if it creates anoter GameManager give it an error message
        {
            Debug.LogError("Error: There Can Only Be One GameManager.");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
