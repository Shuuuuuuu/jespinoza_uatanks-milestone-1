﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum InputScheme { WASD, arrowKeys}; //Input for our controller
    public InputScheme input = InputScheme.WASD; //We will be using the WASD

    private TankMotor motor; //Calls in the TankMotor Script
    private TankData data; //Calls in the TankData Script

    // Start is called before the first frame update
    void Start()
    {
        motor = gameObject.GetComponent<TankMotor>(); //Gets the Component from the TankMotor
        data = gameObject.GetComponent<TankData>(); //Gets the Component from the TankData
    }

    // Update is called once per frame
    void Update()
    {
        switch(input) //Switch statement that will tell the input for our controller
        {
            case InputScheme.WASD: //Calls in the WASD Variables and we will put the usage for the WASD Keys
                if (Input.GetKey(KeyCode.W)) //If we press W the Tank will move up
                {
                    motor.Move(data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.S)) //If we press S the Tank will move down
                {
                    motor.Move(-data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.D)) //If we press D the tank will rotate right
                {
                    motor.Rotate(data.turnSpeed);
                }

                if (Input.GetKey(KeyCode.A)) //If we press A the tank will rotate left
                {
                    motor.Rotate(-data.turnSpeed);
                }

                break; //Breaks the switch statement so it doesn't repeat itself in a whole loop
        }
    }
}
