﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Rigidbody ShellPrefab; //Variable for Rigidbody Bullet
    public Transform FireStart; //Calls the transform for FireStart GameObject
    public float speed; //Variable for the speed
    public float fireRate; //Variable for fireRate;
    public float nextFire; //Variable for nextFire;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFire) //When press space bar it shoots and gives a time out when another bullet will be fired
        {
            nextFire = Time.time + fireRate; //Adds the time and firerate and tells us when is the nextFire available
            Fire(); //Calls the Fire function in our script
        }
        
        void Fire() //Fire Function
        {
            Rigidbody ShellInstance; //Variable for Shell's Rigidbody
            ShellInstance = Instantiate(ShellPrefab, FireStart.position, FireStart.rotation) as Rigidbody; //Makes a Shellclone and where will it appear and rotate depending on where it is fired
            ShellInstance.AddForce(FireStart.forward * speed); //The force that the shell will be fired
        }    
        
    }
}
