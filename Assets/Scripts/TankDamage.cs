﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankDamage : MonoBehaviour
{
    public float damage;  //Damage variable
    public float lifeTime; //Bullet lifetime
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifeTime); //This destroys the bullet and considers its lifetime variable
    }

    void OnTriggerEnter(Collider other) //Calls in a trigger for when bullet hits the collider it disappears
    {
        other.gameObject.GetComponent<TankData>().Damage(damage); //Calls in Damage from the Tank Data Script
        Destroy(gameObject); //Destroys GameObject
    }
}
