﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float moveSpeed = 3; //Variable for moving speed
    public float turnSpeed = 180; //Variable for turn speed
    public float curHealth; //Variable for current Health
    public float maxHealth = 100; //Variable for maximum health

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth; //States that our Health will not go up from the number we add in our maxHealth variable
    }

    // Update is called once per frame
    void Update()
    {

        if(curHealth <= 0) //If statement to see what will happend when our health reaches 0
        {
            Die(); //Calls in the Die Fucntion
        }
    }

    void Die() //Die Function that destroys our GameObject
    {
        Destroy(gameObject); //Destroys GameObject
    }

    public void Damage(float dmg) //Damage function
    {
        curHealth -= dmg; //What will happen when we get hit our curHealth will go down on the number in the damage variable
    }
}
