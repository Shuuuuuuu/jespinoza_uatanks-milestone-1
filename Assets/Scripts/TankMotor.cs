﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    private CharacterController characterController; //Calls in the CharacterController Component
    
    // Start is called before the first frame update
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>(); //Gets the CharacterController Component
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move(float speed) //Move statement that will help our tank to move 
    {
        Vector3 speedVector;
        speedVector = transform.forward;
        speedVector *= speed;
        characterController.SimpleMove(speedVector);
    }

    public void Rotate(float speed) //Rotate statement that will help our tank to rotate
    {
        Vector3 rotateVector;
        rotateVector = Vector3.up;
        rotateVector *= speed;
        rotateVector *= Time.deltaTime;
        transform.Rotate(rotateVector, Space.Self);
    }
}
